Application.$controller("MasterDataNotificationTypePageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };


    $scope.notificationTypeListTableBeforedatarender = function($isolateScope, $data, $columns) {
        $isolateScope.gridOptions.data = undefined;
    };

}]);


Application.$controller("notificationTypeListTableController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.updaterowAction = function($event, $rowData) {
            $scope.Variables.stvCreateNotificationType.dataSet = $rowData;
            $scope.Variables.stvEditNotificationType.dataSet = $rowData;
        };


        $scope.addNewRowAction = function($event) {
            $scope.Variables.stvEditNotificationType.dataSet = {};
        };

    }
]);

Application.$controller("notificationTypeDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.saveButtonClick = function($event, $isolateScope) {
            $scope.Widgets.notificationTypeForm.submit();
        };


        $scope.notificationTypeFormSubmit = function($event, $isolateScope, $formData) {
            // Create notification Type Form
            if ($scope.Variables.stvEditNotificationType.dataSet.id === undefined) {
                $scope.Variables.createNotificationType.setInput('RequestBody', $isolateScope.$root.formatRequestId($scope.Variables.stvCreateNotificationType.dataSet));
                $scope.Variables.createNotificationType.invoke();
            } else {
                //Update notification Type Form
                $scope.Variables.editNotificationType.setInput('RequestBody', $scope.Variables.stvCreateNotificationType.dataSet);
                $scope.Variables.editNotificationType.invoke();
            }
        };

    }
]);