Application.$controller("MasterDataCommentTypePageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };


    $scope.masterDataCommentTypeTableBeforedatarender = function($isolateScope, $data, $columns) {
        $isolateScope.gridOptions.data = undefined;
    };

}]);


Application.$controller("masterDataCommentTypeTableController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.addNewRowAction = function($event) {
            $scope.Variables.stvEditCommentType.dataSet = {};
        };


        $scope.updaterowAction = function($event, $rowData) {
            $scope.Variables.stvCreateCommentType.dataSet = $rowData;
            $scope.Variables.stvEditCommentType.dataSet = $rowData;
        };

    }
]);

Application.$controller("masterCommentTypeDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.saveButtonClick = function($event, $isolateScope) {
            $scope.Widgets.masterCommentTypeForm.submit();
        };


        $scope.masterCommentTypeFormSubmit = function($event, $isolateScope, $formData) {
            //Create comment Type
            if ($scope.Variables.stvEditCommentType.dataSet.id === undefined || $scope.Variables.stvEditCommentType.dataSet.id === "" || $scope.Variables.stvEditCommentType.dataSet.id === null) {
                $scope.Variables.createCommentType.setInput('RequestBody', $isolateScope.$root.formatRequestId($scope.Variables.stvCreateCommentType.dataSet));
                $scope.Variables.createCommentType.invoke();
            } else {
                //Update comment Type
                $scope.Variables.editCommentType.setInput('RequestBody', $scope.Variables.stvCreateCommentType.dataSet);
                $scope.Variables.editCommentType.invoke();
            }
        };

    }
]);