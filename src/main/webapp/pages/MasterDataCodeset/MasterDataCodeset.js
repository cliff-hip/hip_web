Application.$controller("MasterDataCodesetPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

}]);


Application.$controller("masterDataCodesetTableController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.addNewRowAction = function($event) {
            $scope.Variables.stvEditCodeset.dataSet = {}
        };

        $scope.updaterowAction = function($event, $rowData) {
            $scope.Variables.stvEditCodeset.dataSet = $rowData
            $scope.Variables.stvCreateCodeset.dataSet = $rowData
        };

    }
]);

Application.$controller("masterDataCodesetDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.saveButtonClick = function($event, $isolateScope) {
            $scope.Widgets.masterDataCodesetForm.submit();
        };


        $scope.masterDataCodesetFormSubmit = function($event, $isolateScope, $formData) {
            //Create Asset
            if ($scope.Variables.stvEditCodeset.dataSet.id === undefined) {
                $scope.Variables.createCodeset.setInput('RequestBody', formatId($scope.Variables.stvCreateCodeset.dataSet));
                $scope.Variables.createCodeset.invoke();
            } else {
                //Update Asset
                $scope.Variables.editCodeset.setInput('RequestBody', $scope.Variables.stvCreateCodeset.dataSet);
                $scope.Variables.editCodeset.invoke();
            }

        };

        function formatId(codeset) {
            if (codeset.id !== undefined || codeset.id !== null) {
                delete codeset.id;
            }
            return codeset;
        };

    }
]);