Application.$controller("SettingsClientLocationAssetsPageController", ["$scope", "NavigationService", function($scope, NavigationService) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        //Check for Location
        if ($scope.pageParams.locationId === undefined || $scope.pageParams.locationId === "") {
            window.history.back();
        }
    };

    $scope.breadcrumbBeforenavigate = function($isolateScope, $item) {
        NavigationService.goToPage($item.id, {
            'urlParams': $scope.pageParams
        });
        return false;
    };

    $scope.clientLocationAssetListTableDatarender = function($isolateScope, $data) {
        var totalRecords = $scope.Widgets.clientLocationAssetListTable.dataNavigator.dataSize;
        var maxResults = $scope.Widgets.clientLocationAssetListTable.dataNavigator.maxResults;
        var currentPage = $scope.Widgets.clientLocationAssetListTable.dataNavigator.dn.currentPage;

        $scope.paginationPrefix = $scope.$root.getPaginationPrefix(totalRecords, maxResults, currentPage);
    };


    $scope.clientLocationAssetListTableBeforedatarender = function($isolateScope, $data, $columns) {
        $isolateScope.gridOptions.data = undefined;
    };

}]);


Application.$controller("clientLocationAssetListTableController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.addNewRowAction = function($event) {
            $scope.Variables.stvEditclientLocAsset.dataSet = {};
        };


        $scope.updaterowAction = function($event, $rowData) {
            $scope.Variables.stvCreateclientLocAsset.dataSet = $rowData;
            $scope.Variables.stvEditclientLocAsset.dataSet = $rowData;
        };

    }
]);

Application.$controller("dialogAddAssetController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.createClientLocationAssetsFormSubmit = function($event, $isolateScope, $formData) {
            // Create client Asset
            if ($scope.Variables.stvEditclientLocAsset.dataSet.clientLocationAssetId === undefined) {
                $scope.Variables.createClientLocationAsset.setInput('RequestBody', processAssetData($scope.Variables.stvCreateclientLocAsset.dataSet));
                $scope.Variables.createClientLocationAsset.invoke();
            } else {
                //Update client Asset
                $scope.Variables.editClientLocationAsset.setInput('RequestBody', processAssetData($scope.Variables.stvCreateclientLocAsset.dataSet));
                $scope.Variables.editClientLocationAsset.invoke();
            }

        };



        $scope.savelButtonClick = function($event, $isolateScope) {
            $scope.Widgets.createClientLocationAssetsForm.submit();
        };

        // Method Process Data before insert
        function processAssetData(asset) {
            if (asset.hipAsset !== undefined || asset.hipAsset !== null) {
                delete asset.hipAsset;
            }
            if (asset.hipClientLocation !== undefined || asset.hipClientLocation !== null) {
                delete asset.hipClientLocation;
            }
            return asset;
        }

    }
]);