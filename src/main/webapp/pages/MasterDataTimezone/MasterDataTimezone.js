Application.$controller("MasterDataTimezonePageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };



    $scope.timezoneTableBeforedatarender = function($isolateScope, $data, $columns) {
        $isolateScope.gridOptions.data = undefined;
    };

}]);


Application.$controller("timezoneTableController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;


        $scope.updaterowAction = function($event, $rowData) {
            //$scope.Variables.stvEditAsset.dataSet = $rowData
            //$scope.Variables.stvCreateAsset.dataSet = $rowData
        };


        $scope.addNewRowAction = function($event) {
            // $scope.Variables.stvEditAsset.dataSet = {}
        };

    }
]);

Application.$controller("timezoneDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.buttonSaveClick = function($event, $isolateScope) {
            $scope.Widgets.timezoneForm.submit();
        };

        $scope.timezoneFormSubmit = function($event, $isolateScope, $formData) {
            //Create Asset
            if ($scope.Variables.stvEditAsset.dataSet.id === undefined) {
                $scope.Variables.createAsset.setInput('RequestBody', formatId($scope.Variables.stvCreateAsset.dataSet));
                $scope.Variables.createAsset.invoke();
            } else {
                //Update Asset
                $scope.Variables.editAsset.setInput('RequestBody', $scope.Variables.stvCreateAsset.dataSet);
                $scope.Variables.editAsset.invoke();
            }
        };

        function formatId(timezone) {
            if (timezone.id !== undefined || timezone.id !== null) {
                delete timezone.id;
            }
            return timezone;
        };

    }
]);