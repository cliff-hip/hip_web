Application.$controller("SettingsClientLocationContactsPageController", ["$scope", "NavigationService", function($scope, NavigationService) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        // Check for Location
        if ($scope.pageParams.locationId === undefined || $scope.pageParams.locationId === "") {
            window.history.back();
        } else {}
    };





    $scope.contactListTableBeforedatarender = function($isolateScope, $data, $columns) {
        $isolateScope.gridOptions.data = undefined;
    };


    $scope.breadcrumbBeforenavigate = function($isolateScope, $item) {
        NavigationService.goToPage($item.id, {
            'urlParams': $scope.pageParams
        });
        return false;
    };


    $scope.contactListTableDatarender = function($isolateScope, $data) {
        var totalRecords = $scope.Widgets.contactListTable.dataNavigator.dataSize;
        var maxResults = $scope.Widgets.contactListTable.dataNavigator.maxResults;
        var currentPage = $scope.Widgets.contactListTable.dataNavigator.dn.currentPage;

        $scope.paginationPrefix = $scope.$root.getPaginationPrefix(totalRecords, maxResults, currentPage);
    };


    $scope.selectRecordsChange = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.contactListTable.dataNavigator.maxResults = newVal;
        $scope.Widgets.contactListTable.dataNavigator.goToPage();
    };

}]);


Application.$controller("dialogManageContactController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.buttonSaveClick = function($event, $isolateScope) {
            $scope.Widgets.createContactForm.submit();
        };

        // Method Process Data before insert
        function processContactData(contact) {
            // if (contact.hipContactType !== undefined || contact.hipContactType !== null) {
            //     delete contact.hipContactType;
            // }
            // if (contact.hipClientLocation !== undefined || contact.hipClientLocation !== null) {
            //     delete contact.hipClientLocation;
            // }
            delete contact.clientLocationId;
            return contact;
        }

        $scope.createContactFormSubmit = function($event, $isolateScope, $formData) {
            // Create Contact

            if ($scope.Variables.stvEditContact.dataSet.id === undefined) {
                $scope.Variables.createContact.setInput('RequestBody', $isolateScope.$root.formatRequestId($scope.Variables.stvContact.dataSet));
                $scope.Variables.createContact.invoke();
            } else {
                //Update Contact
                $scope.Variables.editContact.setInput('RequestBody', processContactData($scope.Variables.stvContact.dataSet));
                $scope.Variables.editContact.invoke();
            }
        };

    }
]);

Application.$controller("editclientlocationController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("contactListTableController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.updaterowAction = function($event, $rowData) {
            $scope.Variables.stvEditContact.dataSet = $rowData;
            $scope.Variables.stvContact.dataSet = $rowData;
        };


        $scope.addNewRowAction = function($event) {
            $scope.Variables.stvEditContact.dataSet = {};
        };

    }
]);