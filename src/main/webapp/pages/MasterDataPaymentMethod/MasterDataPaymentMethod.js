Application.$controller("MasterDataPaymentMethodPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };


    $scope.masterDataPaymentMethodTableBeforedatarender = function($isolateScope, $data, $columns) {
        $isolateScope.gridOptions.data = undefined;
    };

}]);


Application.$controller("masterDataPaymentMethodTableController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.addNewRowAction = function($event) {
            $scope.Variables.stvEditPaymentMethod.dataSet = {}
            $scope.Variables.stvCreatePaymentMethod.dataSet = {}

        };

        $scope.updaterowAction = function($event, $rowData) {
            $scope.Variables.stvEditPaymentMethod.dataSet = $rowData
            $scope.Variables.stvCreatePaymentMethod.dataSet = $rowData
        };

    }
]);

Application.$controller("masterDataPaymentMethodDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.saveButtonClick = function($event, $isolateScope) {
            $scope.Widgets.masterDataPaymentMethodForm.submit();
        };


        $scope.masterDataPaymentMethodFormSubmit = function($event, $isolateScope, $formData) {
            //Create Asset
            if ($scope.Variables.stvEditPaymentMethod.dataSet.id === undefined) {
                $scope.Variables.createPaymentMethod.setInput('RequestBody', $isolateScope.$root.formatRequestId($scope.Variables.stvCreatePaymentMethod.dataSet));
                $scope.Variables.createPaymentMethod.invoke();
            } else {
                //Update Asset
                $scope.Variables.editPaymentMethod.setInput('RequestBody', $scope.Variables.stvCreatePaymentMethod.dataSet);
                $scope.Variables.editPaymentMethod.invoke();
            }

        };

    }
]);