Application.$controller("integrationPageController", ["$scope", function($scope) {
    "use strict";
    $scope.paginationPrefix = "";
    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };


    $scope.integrationListTableBeforedatarender = function($isolateScope, $data, $columns) {
        $isolateScope.gridOptions.data = undefined;
    };

    $scope.integrationListTableDatarender = function($isolateScope, $data) {
        var totalRecords = $scope.Widgets.integrationListTable.dataNavigator.dataSize;
        var maxResults = $scope.Widgets.integrationListTable.dataNavigator.maxResults;
        var currentPage = $scope.Widgets.integrationListTable.dataNavigator.dn.currentPage;

        $scope.paginationPrefix = $scope.$root.getPaginationPrefix(totalRecords, maxResults, currentPage);
    };

    $scope.textPaginationSizeChange = function($event, $isolateScope, newVal, oldVal) {
        $scope.Variables.clientList.update();
    };

    $scope.selectRecordsChange = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.integrationListTable.dataNavigator.maxResults = newVal;
        $scope.Widgets.integrationListTable.dataNavigator.goToPage();
    };

}]);


Application.$controller("integrationListTableController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.addNewRowAction = function($event) {
            $scope.Variables.stvEditIntegration.dataSet = {};
        };

        $scope.updaterowAction = function($event, $rowData) {
            $scope.Variables.stvCreateIntegration.dataSet = $rowData;
            $scope.Variables.stvEditIntegration.dataSet = $rowData;
        };

    }
]);

Application.$controller("dialogManageIntegrationController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.buttonSaveClick = function($event, $isolateScope) {
            $scope.Widgets.createIntegrationForm.submit();
        };

        $scope.createIntegrationFormSubmit = function($event, $isolateScope, $formData) {
            //Create Integration
            if ($scope.Variables.stvEditIntegration.dataSet.id === undefined || $scope.Variables.stvEditIntegration.dataSet.id === "" || $scope.Variables.stvEditIntegration.dataSet.id === null) {
                $scope.Variables.createIntegration.setInput('RequestBody', $isolateScope.$root.formatRequestId($scope.Variables.stvCreateIntegration.dataSet));
                console.log($scope.Variables.createIntegration);
                $scope.Variables.createIntegration.invoke();
            } else {
                //Update Integration
                $scope.Variables.editIntegration.setInput('RequestBody', $scope.Variables.stvCreateIntegration.dataSet);
                $scope.Variables.editIntegration.invoke();
            }

        };

    }
]);