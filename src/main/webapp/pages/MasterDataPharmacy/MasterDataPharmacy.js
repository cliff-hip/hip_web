Application.$controller("MasterDataPharmacyPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };



    $scope.pharmacyTableBeforedatarender = function($isolateScope, $data, $columns) {

        $isolateScope.gridOptions.data = undefined;
    };

}]);


Application.$controller("pharmacyDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.buttonSaveClick = function($event, $isolateScope) {
            $scope.Widgets.pharmacyForm.submit();
        };



        $scope.pharmacyFormSubmit = function($event, $isolateScope, $formData) {
            // Create Pharmacy Form
            if ($scope.Variables.stvEditPharmacy.dataSet.id === undefined) {
                $scope.Variables.createPharmacy.setInput('RequestBody', $isolateScope.$root.formatRequestId($scope.Variables.stvCreatePharmacy.dataSet));
                $scope.Variables.createPharmacy.invoke();
            } else {
                //Update Pharmacy Form
                $scope.Variables.editPharmacy.setInput('RequestBody', $scope.Variables.stvCreatePharmacy.dataSet);
                $scope.Variables.editPharmacy.invoke();
            }
        };

    }
]);

Application.$controller("pharmacyTableController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.updaterowAction = function($event, $rowData) {
            $scope.Variables.stvEditPharmacy.dataSet = $rowData
            $scope.Variables.stvCreatePharmacy.dataSet = $rowData
        };


        $scope.addNewRowAction = function($event) {
            $scope.Variables.stvEditPharmacy.dataSet = {}
        };

    }
]);