Application.$controller("SettingsClientLocationsPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        // Check for Location
        if ($scope.pageParams.clientId === undefined || $scope.pageParams.clientId === "") {
            window.history.back();
        } else {
            $scope.Variables.locationList.invoke();
        }
    };



    $scope.locationListTableBeforedatarender = function($isolateScope, $data, $columns) {
        $isolateScope.gridOptions.data = undefined;
    };

    $scope.selectRecordsChange = function($event, $isolateScope, newVal, oldVal) {
        // $scope.Variables.locationList.dataBinding.size = newVal;
        // $scope.Variables.locationList.update();
        $scope.Widgets.locationListTable.dataNavigator.maxResults = newVal;
        $scope.Widgets.locationListTable.dataNavigator.goToPage();

    };


    $scope.locationListTableDatarender = function($isolateScope, $data) {
        var totalRecords = $scope.Widgets.locationListTable.dataNavigator.dataSize;
        var maxResults = $scope.Widgets.locationListTable.dataNavigator.maxResults;
        var currentPage = $scope.Widgets.locationListTable.dataNavigator.dn.currentPage;

        $scope.paginationPrefix = $scope.$root.getPaginationPrefix(totalRecords, maxResults, currentPage);
    };


}]);


Application.$controller("editclientlocationController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("locationListTableController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.updaterowAction = function($event, $rowData) {
            $scope.Variables.stvManageLocation.dataSet = $rowData;
            $scope.Variables.stvEditLocation.dataSet = $rowData;
        };


        $scope.addNewRowAction = function($event) {
            $scope.Variables.stvEditLocation.dataSet = {};
        };

    }
]);

Application.$controller("managelocationController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.updateClick = function($event, $isolateScope) {
            $scope.Widgets.locationForm.submit();
        };

        $scope.checkboxPMAsLocChange = function($event, $isolateScope, newVal, oldVal) {
            if (newVal === true)
                $scope.Widgets.textPMLocationName.datavalue = $scope.Widgets.textLocationName.datavalue;
            else
                $scope.Widgets.textPMLocationName.datavalue = '';
        };


        $scope.checkboxShortAsLocChange = function($event, $isolateScope, newVal, oldVal) {
            if (newVal === true)
                $scope.Widgets.textShortName.datavalue = $scope.Widgets.textLocationName.datavalue;
            else
                $scope.Widgets.textShortName.datavalue = '';
        };

        // Method Process Data before insert
        function processData(data) {
            if (data.client !== undefined || data.client !== null) {
                delete data.client;
            }
            // if (data.hipTimezone !== undefined || data.hipTimezone !== null) {
            //     delete data.hipTimezone;
            // }
            delete data.timeZoneId;
            delete data.clientId;
            return data;
        }



        $scope.locationFormSubmit = function($event, $isolateScope, $formData) {
            //create Location
            if ($scope.Variables.stvManageLocation.dataSet.id === undefined || $scope.Variables.stvManageLocation.dataSet.id === null || $scope.Variables.stvManageLocation.dataSet.id === "") {
                $scope.Variables.createLoc.setInput("RequestBody", $scope.Variables.stvManageLocation.dataSet);
                $scope.Variables.createLoc.invoke();
            } else {

                //Edit Location 
                $scope.Variables.editLocation.setInput("RequestBody", processData($scope.Variables.stvEditLocation.dataSet));
                $scope.Variables.editLocation.invoke();
            }
        };





        $scope.parentLocationChange = function($event, $isolateScope, newVal, oldVal) {
            if ($scope.Variables.stvEditLocation.dataSet.id != undefined && $scope.Variables.stvEditLocation.dataSet.id != null && $scope.Variables.stvEditLocation.dataSet.id === newVal) {
                $scope.Variables.stvEditLocation.dataSet.parentLocationId = null;
                $scope.Variables.ngParentLocationCheck.invoke();
            }

        };

    }
]);