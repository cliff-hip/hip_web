Application.$controller("LocationIntegrationConfigPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };

}]);


Application.$controller("stvLocIntegConfigListTableController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("locIntegConfigDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.stvLocIntegConfigFormSubmit = function($event, $isolateScope, $formData) {
            $scope.Widgets.locIntegConfigDialog.close();
        };


        $scope.saveButtonClick = function($event, $isolateScope) {
            $scope.Widgets.stvLocIntegConfigForm.submit();
        };

    }
]);