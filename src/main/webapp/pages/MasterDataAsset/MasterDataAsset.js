Application.$controller("MasterDataAssetPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };


    $scope.assetListTableBeforedatarender = function($isolateScope, $data, $columns) {
        $isolateScope.gridOptions.data = undefined;

    };


    $scope.assetListTableDatarender = function($isolateScope, $data) {
        var totalRecords = $scope.Widgets.assetListTable.dataNavigator.dataSize;
        var maxResults = $scope.Widgets.assetListTable.dataNavigator.maxResults;
        var currentPage = $scope.Widgets.assetListTable.dataNavigator.dn.currentPage;

        $scope.paginationPrefix = $scope.$root.getPaginationPrefix(totalRecords, maxResults, currentPage);
    };

}]);


Application.$controller("assetListTableController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;


        $scope.updaterowAction = function($event, $rowData) {
            $scope.Variables.stvEditAsset.dataSet = $rowData
            $scope.Variables.stvCreateAsset.dataSet = $rowData
        };


        $scope.addNewRowAction = function($event) {
            $scope.Variables.stvEditAsset.dataSet = {}
        };

    }
]);

Application.$controller("manageAssetDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.buttonSaveClick = function($event, $isolateScope) {
            $scope.Widgets.assetForm.submit();
        };

        $scope.assetFormSubmit = function($event, $isolateScope, $formData) {
            //Create Asset
            if ($scope.Variables.stvEditAsset.dataSet.id === undefined) {
                $scope.Variables.createAsset.setInput('RequestBody', formatId($scope.Variables.stvCreateAsset.dataSet));
                $scope.Variables.createAsset.invoke();
            } else {
                //Update Asset
                $scope.Variables.editAsset.setInput('RequestBody', $scope.Variables.stvCreateAsset.dataSet);
                $scope.Variables.editAsset.invoke();
            }
        };

        function formatId(asset) {
            if (asset.id !== undefined || asset.id !== null) {
                delete asset.id;
            }
            return asset;
        };

    }
]);