Application.$controller("MasterDataPayerPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };


    $scope.payerListTableBeforedatarender = function($isolateScope, $data, $columns) {
        $isolateScope.gridOptions.data = undefined;
    };


}]);


Application.$controller("payerListTableController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.updaterowAction = function($event, $rowData) {
            $scope.Variables.stvCreatePayer.dataSet = $rowData;
            $scope.Variables.stvEditPayer.dataSet = $rowData;
        };


        $scope.addNewRowAction = function($event) {
            $scope.Variables.stvEditPayer.dataSet = {}
        };

    }
]);

Application.$controller("payerDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.saveButtonClick = function($event, $isolateScope) {
            $scope.Widgets.payerForm.submit();
        };


        $scope.payerFormSubmit = function($event, $isolateScope, $formData) {
            debugger
            // Create Payer Form
            if ($scope.Variables.stvEditPayer.dataSet.id === undefined) {
                $scope.Variables.createPayer.setInput('RequestBody', $isolateScope.$root.formatRequestId($scope.Variables.stvCreatePayer.dataSet));
                $scope.Variables.createPayer.invoke();
            } else {
                //Update Service Form
                $scope.Variables.editPayer.setInput('RequestBody', $scope.Variables.stvCreatePayer.dataSet);
                $scope.Variables.editPayer.invoke();
            }
        };

    }
]);