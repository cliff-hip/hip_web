Application.$controller("MasterDataServicePageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };


    $scope.serviceListTableBeforedatarender = function($isolateScope, $data, $columns) {
        $isolateScope.gridOptions.data = undefined;
    };

}]);


Application.$controller("serviceListTableController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.updaterowAction = function($event, $rowData) {
            $scope.Variables.stvCreateService.dataSet = $rowData;
            $scope.Variables.stvEditService.dataSet = $rowData;
        };


        $scope.addNewRowAction = function($event) {
            $scope.Variables.stvEditService.dataSet = {}
        };

    }
]);

Application.$controller("serviceDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.saveButtonClick = function($event, $isolateScope) {
            $scope.Widgets.serviceForm.submit();
        };


        $scope.serviceFormSubmit = function($event, $isolateScope, $formData) {
            // Create Service Form
            if ($scope.Variables.stvEditService.dataSet.id === undefined) {
                $scope.Variables.createService.setInput('RequestBody', $isolateScope.$root.formatRequestId($scope.Variables.stvCreateService.dataSet));
                $scope.Variables.createService.invoke();
            } else {
                //Update Service Form
                $scope.Variables.editServices.setInput('RequestBody', $scope.Variables.stvCreateService.dataSet);
                $scope.Variables.editServices.invoke();
            }
        };


    }
]);