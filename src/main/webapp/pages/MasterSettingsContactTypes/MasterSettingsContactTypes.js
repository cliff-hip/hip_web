Application.$controller("MasterSettingsContactTypesPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };



    $scope.contactTypesListonError = function(variable, data, options) {};



    $scope.contactTypesListTableBeforedatarender = function($isolateScope, $data, $columns) {
        $isolateScope.gridOptions.data = undefined;
    };


    $scope.contactTypesListTableDatarender = function($isolateScope, $data) {
        var totalRecords = $scope.Widgets.contactTypesListTable.dataNavigator.dataSize;
        var maxResults = $scope.Widgets.contactTypesListTable.dataNavigator.maxResults;
        var currentPage = $scope.Widgets.contactTypesListTable.dataNavigator.dn.currentPage;

        $scope.paginationPrefix = $scope.$root.getPaginationPrefix(totalRecords, maxResults, currentPage);
    };

}]);


Application.$controller("contactTypesListTableController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.addNewRowAction = function($event) {
            $scope.Variables.stvEditContactType.dataSet = {};
        };

        $scope.updaterowAction = function($event, $rowData) {
            $scope.Variables.stvEditContactType.dataSet = $rowData;
            $scope.Variables.stvCreateContactType.dataSet = $rowData;
        };

    }
]);

Application.$controller("contactTypeDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.createContactTypeFormSubmit = function($event, $isolateScope, $formData) {
            // Create Contact Type
            if ($scope.Variables.stvEditContactType.dataSet.contactTypeId === undefined) {
                $scope.Variables.createContactType.setInput('RequestBody', formatRequest($scope.Variables.stvCreateContactType.dataSet));
                $scope.Variables.createContactType.invoke();
            } else {
                //Update Contact Type
                $scope.Variables.editContactType.setInput('RequestBody', $scope.Variables.stvCreateContactType.dataSet);
                $scope.Variables.editContactType.invoke();
            }
        };

        function formatRequest(variable) {
            if (variable.id != undefined) {
                delete variable.id;
            }
            return variable;
        }

        $scope.saveButtonClick = function($event, $isolateScope) {
            $scope.Widgets.createContactTypeForm.submit();
        };

    }
]);